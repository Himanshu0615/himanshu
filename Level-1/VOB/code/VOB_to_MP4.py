# Import everything needed to edit video clips
from moviepy.editor import *
import ffmpy
#ffmpeg = "C:\\Program Files\\ImageMagick-7.0.1-Q16\\ffmpeg"

# Read file
#clip = VideoFileClip("D:\AcuitiChallenge\repos\acuiti-hiring-hack\Level-1\VOB\input\BOV.vob")

ff = ffmpy.FFmpeg(
    inputs={r'D:\AcuitiChallenge\repos\acuiti-hiring-hack\Level-1\VOB\input\BOV.vob': None},
    outputs={r'D:\AcuitiChallenge\repos\acuiti-hiring-hack\Level-1\VOB\output\VOB_to_MP4.mp4': None}
)
ff.run()

# Reduce the audio volume (volume x 0.8)
#clip = clip.volumex(0.8)

# Generate a text clip. You can customize the font, color, etc.
#txt_clip = TextClip("MyConvertedClip")

# Overlay the text clip on the first video clip
#video = CompositeVideoClip([clip, txt_clip])

# Write the result to a file (many options available !)
#video.write_videofile("D:\AcuitiChallenge\repos\acuiti-hiring-hack\Level-1\VOB\output\VOBtoMP4.mp4")
