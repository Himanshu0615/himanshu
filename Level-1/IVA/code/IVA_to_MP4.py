# Import everything needed to edit video clips
from moviepy.editor import *
import os.path
import ffmpy
import gi
from gi.repository import Gst
gi.require_version('Gst', '1.0')
import moviepy
import os
Gst.init(None)
import glob

# Get the present working Directory value

present_dir = os.getcwd()

# [ The complete list of dir/files is collected in x]
x = os.listdir('D:\\AcuitiChallenge\\repos\\acuiti-hiring-hack\\Level-1\\IVA\\input\\IVA')
a = set()

for p in (x):
    a.add(p)

Home_Dir='D:\\AcuitiChallenge\\repos\\acuiti-hiring-hack\\Level-1\\IVA\\input\\IVA'
#Loop through the directories one by one and store the .mp4 files in the output Directory.


count = 0
for y in (a):
    z = Home_Dir + "\\" + y
    os.chdir(z)
    a = os.listdir(z)
    for b in (a):
        final_dir = z + "\\" + b
        os.chdir(final_dir)
        fil=final_dir + "\\" + "Output.mp4"
        if(os.path.exists(fil)):
            os.remove(fil)
        vid = os.listdir(final_dir)
        ff = ffmpy.FFmpeg(
            inputs={vid[1]: None},
            outputs={r'Output.mp4': None}
            )
        ff.run()
        c = count
        fc= "D:\\AcuitiChallenge\\repos\\acuiti-hiring-hack\\Level-1\\IVA\\output\\" + `c` + "Output.mp4"
        os.rename(fil,fc)
        count +=1
    os.chdir(present_dir)


#```Go to the Output Directory```

os.chdir("D:\\AcuitiChallenge\\repos\\acuiti-hiring-hack\\Level-1\\IVA\\output")

out_dir=os.getcwd()
vidfin= out_dir + "\\" + "Final.mp4"
if (os.path.exists(vidfin)):
    os.remove(vidfin)
arr =[]

#```Make an array of the video clips```
for v1 in glob.glob("*.mp4"):
    arr.append(v1)


#```Concatenate the Video clips```
final_clip = concatenate_videoclips([VideoFileClip(clip).set_duration(8) for clip in arr],method = "compose")
final_clip.write_videofile("Final.mp4")
for out in glob.glob("*Output.mp4"):
    os.remove(out)


    
